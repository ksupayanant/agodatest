# Download

## Repository
```
https://bitbucket.org/ksupayanant/agodatest/src/master/
```

## local Install

```sh
    $ sbt compile
```


## Usage
You can run using command line

Reverse String function
```sh
      $ sbt "run 1 \"This is a reverse string function\""
```
Shingle filter function
```sh
      $ sbt "run 2 \"This is a shinglefilter function\""
```

IntelliJ Idea
```sh
      You can open with IntelliJ Idea and run it
```

## Test

Run All Tests
```sh
      $ sbt test
```

Run Reverse String Tests
```sh
       $ sbt "test:testOnly *ReverseStringTest"
```

Run Shingle Filter Tests
```sh
       $ sbt "test:testOnly *ShingleFilterTest"
```

## Query

```sql
SELECT DISTINCT actor.NAME
FROM   actor
       INNER JOIN movie_actor ma
               ON actor.id = ma.actor_id
WHERE  id IN (SELECT ma.actor_id
              FROM   movie m
                     INNER JOIN movie_actor ma
                             ON m.id = ma.movie_id
              WHERE  m.year = 2001)
       AND id NOT IN (SELECT ma.actor_id
                      FROM   movie m
                             INNER JOIN movie_actor ma
                                     ON m.id = ma.movie_id
                      WHERE  m.year = 2002)
       AND Lower(ma.role_name) = 'starred'
```

Note: SQL File is located in resources