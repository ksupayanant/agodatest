import ReverseString.ReverseString
import ShingleFilter.ShingleFilter

/**
  * Created by kornkritsupayanant on 2/1/2019 AD.
  */
object Main {
  def main(args: Array[String]) : Unit = {
    try {
      if (args.length > 1) {
        if (args(0) == "1") {
          val reverseString: ReverseString = new ReverseString
          println(reverseString.reverse(args(1)))
        } else if (args(0) == "2") {
          val shingleFilter: ShingleFilter = new ShingleFilter
          val setOfFilter: List[String] = shingleFilter.filter(args(1)).toList
          for (i <- 0 until setOfFilter.size) {
            println(setOfFilter(i))
          }
        } else {
          println("Doesn't have this method")
        }
      }
    }catch {
      case e: Exception => println(e.toString)
    }
  }
}
