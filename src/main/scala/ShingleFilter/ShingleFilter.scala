package ShingleFilter

/**
  * Created by kornkritsupayanant on 29/12/2018 AD.
  */
class ShingleFilter {
  def filter(text: String, textLeft: String = "", resultSet: Set[String] = Set(), sizeOfAdjacent: Integer = 1, pivot: Integer = 0, textToAdd: String = ""): Set[String] = {
    if(sizeOfAdjacent == text.split(" ").length){
      return resultSet ++ Set(text)
    }else{
      if(sizeOfAdjacent == 1) {
        val tmpResultSet = text.split(" ").toSet[String]
        filter(text,text,tmpResultSet, sizeOfAdjacent + 1)
      }else{
        val wordToAdd = trim(textToAdd + " " + textLeft.substring(0, if(textLeft.indexOf(" ") < 0) textLeft.length else textLeft.indexOf(" ")))
        val tmpTextLeft = trim(textLeft.substring(if(textLeft.indexOf(" ") < 0) 0 else textLeft.indexOf(" "), textLeft.length))

        if (sizeOfAdjacent == ((text.split(" ").length - textLeft.split(" ").length) - (pivot - 1))) {
          val tmpResultSet = resultSet ++ Set(wordToAdd)
          if(textLeft.split(" ").length == 1){
            filter(text, text, tmpResultSet, sizeOfAdjacent + 1, 0, "")
          }else {
            val tmpTextLeftAFP = trim(text.substring(getIndexOfPivot(text,pivot + 1), text.length))
            filter(text, tmpTextLeftAFP, tmpResultSet, sizeOfAdjacent, pivot + 1, "")
          }
        }else{
          filter(text, tmpTextLeft, resultSet, sizeOfAdjacent, pivot, wordToAdd)
        }
      }
    }
  }

  def trim(text: String): String = {
    text.replaceAll("^\\s+|\\s+$","")
  }

  def getIndexOfPivot(text: String, startPivot: Integer, textLeft: String = "", pivot: Integer = 0): Integer = {
    if(pivot == text.split(" ").length) {
      text.length - trim(textLeft).length
    }else{
      val tmpText = text.split(" ")
      val tmpPivot: Integer = pivot.intValue()
      if(tmpPivot >= startPivot) {
        getIndexOfPivot(text, startPivot, trim(textLeft + " " + tmpText(pivot)), pivot + 1)
      }else{
        getIndexOfPivot(text, startPivot, "", pivot + 1)
      }
    }
  }
}
