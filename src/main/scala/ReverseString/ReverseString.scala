package ReverseString

/**
  * Created by kornkritsupayanant on 28/12/2018 AD.
  */
class ReverseString {
  def reverse(sentence: String,reversedSentence: String = "", connector: String = ""): String = {
    if(trim(sentence).length == 0)
      return sentence


    val sentences: Array[String] = sentence.split("\\s")
    val result: String = reversedSentence + connector + sentences(sentences.length - 1)
    if(sentences.length == 1) {
      result
    }else{
      reverse(sentence.substring(0, getLastIndexOf(sentence)), result, getConnector(sentence))
    }
  }

  def trim(text: String): String = {
    text.replaceAll("^\\s+|\\s+$","")
  }

  def getConnector(text: String, connector: String = "", isFound: Boolean = false): String = {
    val lastIndex = text.length - 1

    if(isFound && (text.charAt(lastIndex) != ' ' && text.charAt(lastIndex) != '\n' && text.charAt(lastIndex) != '\t')){
      return connector
    }else{
      if(text.charAt(lastIndex) == ' ' || text.charAt(lastIndex) == '\n' || text.charAt(lastIndex) == '\t'){
        val result = connector + text.charAt(lastIndex)
        getConnector(text.substring(0,lastIndex), result, true)
      }else{
        getConnector(text.substring(0,lastIndex))
      }
    }
  }

  def getLastIndexOf(text: String): Integer = {
    val len = text.length
    for(i <- (len - 1) to (0,-1)){
      if((text.charAt(i).equals(' ') || text.charAt(i).equals('\n') || text.charAt(i).equals('\t')) &&
        (!text.charAt(i-1).equals(' ') && !text.charAt(i-1).equals('\n') && !text.charAt(i-1).equals('\t'))) {
        return i
      }
    }
    len - 1
  }


}