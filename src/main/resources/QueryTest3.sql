SELECT DISTINCT actor.NAME 
FROM   actor 
       INNER JOIN movie_actor ma 
               ON actor.id = ma.actor_id 
WHERE  id IN (SELECT ma.actor_id 
              FROM   movie m 
                     INNER JOIN movie_actor ma 
                             ON m.id = ma.movie_id 
              WHERE  m.year = 2001) 
       AND id NOT IN (SELECT ma.actor_id 
                      FROM   movie m 
                             INNER JOIN movie_actor ma 
                                     ON m.id = ma.movie_id 
                      WHERE  m.year = 2002) 
       AND Lower(ma.role_name) = 'starred' 