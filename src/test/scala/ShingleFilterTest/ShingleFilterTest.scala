package ShingleFilterTest

import ShingleFilter.ShingleFilter
import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by kornkritsupayanant on 29/12/2018 AD.
  */
class ShingleFilterTest extends FlatSpec with Matchers {
  "Shingle are effectively word-nGrams" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "Shingle are effectively word-nGrams"
    val resultSet = Set(
      "Shingle",
      "are",
      "effectively",
      "word-nGrams",
      "Shingle are",
      "Shingle are effectively",
      "Shingle are effectively word-nGrams",
      "are effectively",
      "are effectively word-nGrams",
      "effectively word-nGrams")
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "This is cat" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "This is cat"
    val resultSet = Set(
      "This",
      "is",
      "cat",
      "This is",
      "is cat",
      "This is cat")
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "This is my brown dog" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "This is my brown dog"
    val resultSet = Set(
      "This",
      "is",
      "my",
      "brown",
      "dog",
      "This is",
      "is my",
      "my brown",
      "brown dog",
      "This is my",
      "is my brown",
      "my brown dog",
      "This is my brown",
      "is my brown dog",
      "This is my brown dog"
    )
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "Gear" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "Gear"
    val resultSet = Set(
      "Gear"
    )
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "Empty" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = ""
    val resultSet = Set(
      ""
    )
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "A B C D" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "A B C D"
    val resultSet = Set(
      "A",
      "B",
      "C",
      "D",
      "A B",
      "B C",
      "C D",
      "A B C",
      "B C D",
      "A B C D"
    )
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "I love coding in scala programing" should "be match the set" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "I love coding in scala programing"
    val resultSet = Set(
      "I",
      "love",
      "coding",
      "in",
      "scala",
      "programing",
      "I love",
      "love coding",
      "coding in",
      "in scala",
      "scala programing",
      "I love coding",
      "love coding in",
      "coding in scala",
      "in scala programing",
      "I love coding in",
      "love coding in scala",
      "coding in scala programing",
      "I love coding in scala",
      "love coding in scala programing",
      "I love coding in scala programing"
    )
    shingleFilter.filter(sentence) should be (resultSet)
  }

  "getIndexOf A B C and pivot equals to 1" should "2" in {
    val shingleFilter = new ShingleFilter()
    val sentence = "A B C"
    val resultSet = 2
    shingleFilter.getIndexOfPivot(sentence, 1) should be (resultSet)
  }

  " This should be trimmed " should "This should be trimmed" in {
    val shingleFilter = new ShingleFilter()
    val sentence = " This should be trimmed "
    val resultSet = "This should be trimmed"
    shingleFilter.trim(sentence) should be (resultSet)
  }

}
