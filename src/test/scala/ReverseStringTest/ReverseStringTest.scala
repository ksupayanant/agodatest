package ReverseStringTest

import ReverseString.ReverseString
import org.scalatest._

/**
  * Created by kornkritsupayanant on 28/12/2018 AD.
  */
class ReverseStringTest extends FlatSpec with Matchers{

  "this is a test" should "be test a is this" in {
    val sentence = "this is a test"
    val result = "test a is this"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "Multi-lines\tstring\t with\ntabs" should "be tabs\nwith \tstring\tMulti-lines" in {
    val sentence = "Multi-lines\tstring\t with\ntabs"
    val result = "tabs\nwith \tstring\tMulti-lines"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "Multi-lines\tstring\t  with\ntabs" should "be tabs\nwith  \tstring\tMulti-lines" in {
    val sentence = "Multi-lines\tstring\t  with\ntabs"
    val result = "tabs\nwith  \tstring\tMulti-lines"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "Empty" should "be Empty" in {
    val sentence = ""
    val result = ""
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "CatAndDog" should "be CatAndDog" in {
    val sentence = "CatAndDog"
    val result = "CatAndDog"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "This is my dog" should "dog my is This" in {
    val sentence = "This is my dog"
    val result = "dog my is This"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "This \t\nis" should "be is\n\t This" in {
    val sentence = "This \t\nis"
    val result = "is\n\t This"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "A  B  \tC D  E    F G" should "be G F    E  D C\t  B  A" in {
    val sentence = "A  B  \tC D  E    F G"
    val result = "G F    E  D C\t  B  A"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  "   " should "be    " in {
    val sentence = "   "
    val result = "   "
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.reverse(sentence) should be (result)
  }

  " This is trim function " should "This is trim function" in {
    val sentence = " This is trim function "
    val result = "This is trim function"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.trim(sentence) should be (result)
  }

  " This is ltrim function" should "This is ltrim function" in {
    val sentence = " This is ltrim function"
    val result = "This is ltrim function"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.trim(sentence) should be (result)
  }

  "This is rtrim function " should "This is rtrim function" in {
    val sentence = "This is rtrim function "
    val result = "This is rtrim function"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.trim(sentence) should be (result)
  }

  "1. This should return\n" should "\n" in {
    val sentence = "This should return\n"
    val result = "\n"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.getConnector(sentence) should be (result)
  }

  "2. This should return\n\t" should "\t\n" in {
    val sentence = "This should return\n\t"
    val result = "\t\n"
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.getConnector(sentence) should be (result)
  }

  "This should return\n\t" should "18" in {
    val sentence = "This should return\n\t"
    val result = 18
    val reverseObj : ReverseString  = new ReverseString()
    reverseObj.getLastIndexOf(sentence) should be (result)
  }

}
